/*
 * =====================================================================================
 *
 *       Filename:  LivenessAnalysis.cpp
 *
 *    Description:  LLVM Pass to check the liveness of variables
 *
 *        Version:  1.0
 *        Created:  03/12/2018 02:05:15
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Sai Kishan Pampana (Pampana), saikishanpampana@gmail.com
 *   Organization:  University of California San Diego
 *
 * =====================================================================================
 */

#include "llvm/InitializePasses.h"
#include "llvm/Pass.h"
#include "llvm/IR/CFG.h"
#include "llvm/IR/InstIterator.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Function.h"
#include "llvm/Support/raw_ostream.h"
#include <deque>
#include <map>
#include <utility>
#include <vector>
#include "231DFA.h"
#include <set>
#include "llvm/IR/Instruction.h"
#include "llvm/Support/raw_ostream.h"
#include <string>

using namespace llvm;

namespace
{
	// Creating class LiveInfo, subclass of class Info
	class LiveInfo : public Info
	{
		public:
			void print()
			{
				for(std::set<unsigned>::iterator i = indices.begin(); i != indices.end(); i++)
					errs() << *i << "|";
				errs() << "\n";

			}

			static bool equals(LiveInfo * info1, LiveInfo * info2)
			{
				if(info1->indices == info2->indices)
					return true;

				return false;
			}
			
			
			static LiveInfo* join(LiveInfo * info1, LiveInfo * info2, LiveInfo * result)
			{
				// To allocate the object on the heap so that we avoid loosing data on deletion of function stack
				for(std::set<unsigned>::iterator i = info1->indices.begin(); i != info1->indices.end(); i++)
					result->indices.insert(*i);

				for(std::set<unsigned>::iterator i = info2->indices.begin(); i != info2->indices.end(); i++)
					result->indices.insert(*i);

				return result;

			}

			std::set<unsigned> indices;

	};

	// Creating class ReachingDefinitionAnalysis, subclass of class DataFlowAnalysis
	class LivenessAnalysis: public DataFlowAnalysis<LiveInfo, false>
	{
		public : 
			// Constructor function for the class
			LivenessAnalysis(LiveInfo &bottom, LiveInfo &initialState):DataFlowAnalysis<LiveInfo, false>::DataFlowAnalysis(bottom, initialState){} 

			virtual void flowfunction(Instruction *I, std::vector<unsigned> &IncomingEdges, std::vector<unsigned> &OutgoingEdges, std::vector<LiveInfo *> &Infos)
			{
				if(I == nullptr)
					return;
				std::string string_name = I->getOpcodeName();
				unsigned instr_index = this->InstrToIndex[I];		// Getting the index of the present instruction
				LiveInfo * indices_to_add = new LiveInfo(); 

				// Joining the information for all the incoming edges;
				for(std::vector<unsigned>::iterator i = IncomingEdges.begin(); i != IncomingEdges.end(); i++)
				{
					Edge edge = std::make_pair(*i, instr_index);
					indices_to_add = LiveInfo::join(EdgeToInfo[edge], indices_to_add, indices_to_add);
				}


				// Case when the instruction we encounter is a phi node
				//if(std::string(I->getOpcodeName()) == "phi")
				//{
				//	while(1)	// Keep looping till the instruction we find is not a phi node
				//	{
				//		indices_to_add->indices.insert(instr_index);
				//		instr_index++;

				//		string_name = this->IndexToInstr[instr_index]->getOpcodeName();
				//		if(string_name != "phi")
				//			break;

				//	}
				//}

				// Checking for all the binary operations
				if(I->isBinaryOp())
				{
					// Erasing the index as we are returning a value in a binary operation
					indices_to_add->indices.erase(instr_index);
					
					// Including the index of the operands used in the instruction
					unsigned operand_count = I->getNumOperands();
					for(unsigned i = 0; i < operand_count; i++)
					{
						Instruction * operand_instr = (Instruction *)I->getOperand(i);
						if(this->InstrToIndex.find(operand_instr) != this->InstrToIndex.end())
						{
							unsigned operand_index = this->InstrToIndex[operand_instr];
							indices_to_add->indices.insert(operand_index);
						}

					}
					
				}

				// Checking for all the instructions opcodes which are of the first category
				else if (string_name == "icmp" || string_name == "fcmp" || string_name == "alloca" || string_name == "load" || string_name == "getelementptr" || string_name == "select")
				{
					// Adding indices of the operands that are used in the instructions
					indices_to_add->indices.erase(instr_index);
					unsigned operand_count = I->getNumOperands();
					for(unsigned i = 0; i < operand_count; i++)
					{
						Instruction * operand_instr = (Instruction *)I->getOperand(i);
						if(this->InstrToIndex.find(operand_instr) != this->InstrToIndex.end())
						{
							unsigned operand_index = this->InstrToIndex[operand_instr];
							indices_to_add->indices.insert(operand_index);
						}
					}
				}

				else if(string_name != "phi")
				{
					unsigned operand_count = I->getNumOperands();
					for(unsigned i = 0; i < operand_count; i++)
					{
						Instruction * operand_instr = (Instruction *)I->getOperand(i);
						if(this->InstrToIndex.find(operand_instr) != this->InstrToIndex.end())
						{
							unsigned operand_index = this->InstrToIndex[operand_instr];
							indices_to_add->indices.insert(operand_index);
						}
					}

				}
				for(std::vector<LiveInfo *>::iterator i = Infos.begin(); i != Infos.end(); i++)
				{
					(*i)->indices = indices_to_add->indices;
				}

				unsigned temp = instr_index;

				for(std::vector<LiveInfo *>::iterator i = Infos.begin(); i != Infos.end(); i++)
				{
					//	(*i)->indices = indices_to_add->indices;
					instr_index = temp;
					unsigned outgoing_edge = OutgoingEdges[std::distance(Infos.begin(), i)];
					if(std::string(I->getOpcodeName()) == "phi")
					{
						//errs() << "The out going edge " << outgoing_edge << " Distance from begin " << std::distance(Infos.begin(), i) <<  "\n";
						unsigned counting = 0;
						while(1)
						{
							//(*i)->indices.erase(instr_index);
							//instr_index++;
							//string_name = this->IndexToInstr[instr_index]->getOpcodeName();
							//if(string_name != "phi")
							//	break;
							if(isa<PHINode>(IndexToInstr[instr_index]))
							{
								(*i)->indices.erase(instr_index);
								instr_index++;
							}

							else
								break;

						}
						instr_index = temp;
						while(1)
						{
							//errs() << "Hey " << counting <<" Index = " <<instr_index <<  "\n";
							counting += 1;
							// Removing the index of the phi node from the set
						//	(*i)->indices.erase(instr_index);
							PHINode *phi_node_instr = (PHINode *)IndexToInstr[instr_index];
							//Instruction * instr = this->IndexToInstr[instr_index];
							//unsigned operand_count = instr->getNumOperands();
							unsigned operand_count = phi_node_instr->getNumIncomingValues();

							for(unsigned j = 0; j < operand_count; j++)
							{
								//Instruction * operand_instr = (Instruction *) instr->getOperand(j);
								Instruction * operand_instr = (Instruction*)(phi_node_instr->getIncomingValue(j));
								//if(InstrToIndex.find(operand_instr) != InstrToIndex.end())
								//	errs() << "The parents of this operand is " << operand_instr->getParent() << "\n";

								//if(IndexToInstr.find(outgoing_edge) != IndexToInstr.end() && IndexToInstr[outgoing_edge] != nullptr)
								//	errs() << "The parents of the outgoing edge is " << this->IndexToInstr[outgoing_edge]->getParent() << "\n";

								BasicBlock *data_source_block = phi_node_instr->getIncomingBlock(j);
								Instruction * block_last_instr = (Instruction *)data_source_block->getTerminator();
								unsigned value_index = InstrToIndex[block_last_instr];


								if(InstrToIndex.find(operand_instr) != InstrToIndex.end() && IndexToInstr.find(outgoing_edge) != IndexToInstr.end() && IndexToInstr[outgoing_edge] != nullptr && value_index == outgoing_edge) //operand_instr->getParent() == this->IndexToInstr[outgoing_edge]->getParent())
								{
								//	errs() << "DAMN IT" << "\n\n\n";
									(*i)->indices.insert(this->InstrToIndex[operand_instr]);
								}

							}


							instr_index++;

							string_name = this->IndexToInstr[instr_index]->getOpcodeName();
							if(string_name != "phi")
								break;
						}
					}
				}


				delete indices_to_add;
			}



	};


	// Constructing the Pass needed for the liveness analysis
	struct LivenessAnalysisPass:public FunctionPass{
		static char ID;
		LivenessAnalysisPass():FunctionPass(ID){}

		// Running the function so that we call the flow fucntion
		bool runOnFunction(Function &F) override{
			LiveInfo bottom;		// Bottom would be an empty set of liveness info
			LivenessAnalysis liveness_analysis(bottom, bottom); 

			liveness_analysis.runWorklistAlgorithm(&F);
			liveness_analysis.print();

			return false;


		}
	};
}

char LivenessAnalysisPass::ID = 0;
static RegisterPass<LivenessAnalysisPass> X("cse231-liveness", "LivenessAnalysisPass", false, false);




