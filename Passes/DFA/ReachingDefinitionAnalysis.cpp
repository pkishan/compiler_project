/*
 * =====================================================================================
 *
 *       Filename:  ReachingDefinitionAnalysis.cpp
 *
 *    Description:  Code involving the reaching definition analysis for LLVM Project part 2
 *
 *        Version:  1.0
 *        Created:  02/25/2018 19:39:17
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Sai Kishan Pampana (Pampana), saikishanpampana@gmail.com
 *   Organization:  University of California San Diego
 *
 * =====================================================================================
 */

#include "llvm/InitializePasses.h"
#include "llvm/Pass.h"
#include "llvm/IR/CFG.h"
#include "llvm/IR/InstIterator.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Function.h"
#include "llvm/Support/raw_ostream.h"
#include <deque>
#include <map>
#include <utility>
#include <vector>
#include "231DFA.h"
#include <set>
#include "llvm/IR/Instruction.h"
#include "llvm/Support/raw_ostream.h"
#include <string>

using namespace llvm;

namespace
{
	// Creating class ReachingInfo, subclass of class Info
	class ReachingInfo : public Info
	{
		public:
			void print()
			{
				for(std::set<unsigned>::iterator i = indices.begin(); i != indices.end(); i++)
					errs() << *i << "|";
				errs() << "\n";

			}

			static bool equals(ReachingInfo * info1, ReachingInfo * info2)
			{
				if(info1->indices == info2->indices)
					return true;

				return false;
			}
			
			
			static ReachingInfo* join(ReachingInfo * info1, ReachingInfo * info2, ReachingInfo * result)
			{
				// To allocate the object on the heap so that we avoid loosing data on deletion of function stack
				for(std::set<unsigned>::iterator i = info1->indices.begin(); i != info1->indices.end(); i++)
					result->indices.insert(*i);

				for(std::set<unsigned>::iterator i = info2->indices.begin(); i != info2->indices.end(); i++)
					result->indices.insert(*i);

				return result;

			}

			std::set<unsigned> indices;

	};

	// Creating class ReachingDefinitionAnalysis, subclass of class DataFlowAnalysis
	class ReachingDefinitionAnalysis : public DataFlowAnalysis<ReachingInfo, true>
	{
		//private:
		//	ReachingInfo Bottom;
		//	ReachingInfo InitialState;
		//	Instruction *EntryInstr; 
		public : 
			// Constructor function for the class
			ReachingDefinitionAnalysis(ReachingInfo &initialState, ReachingInfo &bottom):DataFlowAnalysis<ReachingInfo, true>::DataFlowAnalysis(bottom, initialState){} 

			virtual void flowfunction(Instruction *I, std::vector<unsigned> &IncomingEdges, std::vector<unsigned> &OutgoingEdges, std::vector<ReachingInfo *> &Infos)
			{
				if(I == nullptr)
					return;
				std::string string_name = I->getOpcodeName();
				unsigned instr_index = this->InstrToIndex[I];		// Getting the index of the present instruction
				ReachingInfo * indices_to_add = new ReachingInfo(); 

				// Joining the information for all the incoming edges;
				for(std::vector<unsigned>::iterator i = IncomingEdges.begin(); i != IncomingEdges.end(); i++)
				{
					Edge edge = std::make_pair(*i, instr_index);
					indices_to_add = ReachingInfo::join(EdgeToInfo[edge], indices_to_add, indices_to_add);
				}


				// Case when the instruction we encounter is a phi node
				if(std::string(I->getOpcodeName()) == "phi")
				{
					while(1)	// Keep looping till the instruction we find is not a phi node
					{
						indices_to_add->indices.insert(instr_index);
						instr_index++;

						string_name = this->IndexToInstr[instr_index]->getOpcodeName();
						if(string_name != "phi")
							break;

					}
				}

				// Checking for all the binary operations
				if(I->isBinaryOp())
				{
					indices_to_add->indices.insert(instr_index);
					
				}

				// Checking for all the instructions opcodes which are of the first category
				if (string_name == "icmp" || string_name == "fcmp" || string_name == "alloca" || string_name == "load" || string_name == "getelementptr" || string_name == "select")
				{
					indices_to_add->indices.insert(instr_index);
				}

				for(std::vector<ReachingInfo *>::iterator i = Infos.begin(); i != Infos.end(); i++)
				{
					(*i)->indices = indices_to_add->indices;
				}


				delete indices_to_add;
			}



	};


	// Constructing the Pass needed for the reaching definition analyssi
	struct ReachingDefinitionAnalysisPass:public FunctionPass{
		static char ID;
		ReachingDefinitionAnalysisPass():FunctionPass(ID){}

		// Running the function so that we call the flow fucntion
		bool runOnFunction(Function &F) override{
			ReachingInfo bottom;		// Bottom would be an empty set of reaching defintions
			ReachingDefinitionAnalysis reaching_analysis(bottom, bottom); 

			reaching_analysis.runWorklistAlgorithm(&F);
			reaching_analysis.print();

			return false;


		}
	};
}

char ReachingDefinitionAnalysisPass::ID = 0;
static RegisterPass<ReachingDefinitionAnalysisPass> X("cse231-reaching", "ReachingDefinitionAnalysisPass", false, false);




