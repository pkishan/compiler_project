/*
 * =====================================================================================
 *
 *       Filename:  MayPointToAnalysis.cpp
 *
 *    Description:  May point to analysis for part 3 of the cse 231 project
 *
 *        Version:  1.0
 *        Created:  03/21/2018 15:39:03
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Sai Kishan Pampana (Pampana), saikishanpampana@gmail.com
 *   Organization:  University of California San Diego
 *
 * =====================================================================================
 */

#include "llvm/InitializePasses.h"
#include "llvm/Pass.h"
#include "llvm/IR/CFG.h"
#include "llvm/IR/InstIterator.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Function.h"
#include "llvm/Support/raw_ostream.h"
#include <deque>
#include <map>
#include <utility>
#include <vector>
#include "231DFA.h"
#include <set>
#include "llvm/IR/Instruction.h"
#include "llvm/Support/raw_ostream.h"
#include <string>

using namespace llvm;

namespace
{
	// Creating class MayPointToInfo, subclass of class Info
	class  MayPointToInfo: public Info
	{
		public:
			void print()
			{
				for(std::map<std::string, std::set<std::string>>::iterator i = indices.begin(); i != indices.end(); i++)
				{
					if(i->second.size() != 0)
					{
						errs() << i->first << "->" << "(";
						for(std::set<std::string>::iterator j = i->second.begin(); j != i->second.end(); j++)
							errs() << *j << "/";
						errs() << ")|" ;
					}
					
				}
				//	errs() << *i << "|";
				errs() << "\n";

			}

			static bool equals(MayPointToInfo* info1, MayPointToInfo* info2)
			{
				if(info1->indices.size() != info2->indices.size())
					return false;
				for(std::map<std::string, std::set<std::string>>::iterator i = info1->indices.begin(); i != info1->indices.end(); i++)
				{
					if(info2->indices.count(i->first) == 0)
						return false;
					else if(info1->indices[i->first] != info2->indices[i->first])	// Comparing the sets each element of the map holds
						return false;

				}

				return true;
			}
			
			
			static MayPointToInfo* join(MayPointToInfo * info1, MayPointToInfo* info2, MayPointToInfo* result)
			{
				// To allocate the object on the heap so that we avoid loosing data on deletion of function stack
//				for(std::set<unsigned>::iterator i = info1->indices.begin(); i != info1->indices.end(); i++)
//					result->indices.insert(*i);
//
//				for(std::set<unsigned>::iterator i = info2->indices.begin(); i != info2->indices.end(); i++)
//					result->indices.insert(*i);
				for(std::map<std::string, std::set<std::string>>::iterator i = info1->indices.begin(); i != info1->indices.end(); i++)
				{
					if(result->indices.count(i->first) == 0)
						result->indices[i->first] = i->second;
					else
					{
						result->indices[i->first].insert(info1->indices[i->first].begin(), info1->indices[i->first].end());

					}
				}

				for(std::map<std::string, std::set<std::string>>::iterator i = info2->indices.begin(); i != info2->indices.end(); i++)
				{
					if(result->indices.count(i->first) == 0)
						result->indices[i->first] = i->second;
					else
					{
						result->indices[i->first].insert(info2->indices[i->first].begin(), info2->indices[i->first].end());

					}
				}

				return result;

			}

			std::map<std::string, std::set<std::string>> indices;

	};

	// Creating class ReachingDefinitionAnalysis, subclass of class DataFlowAnalysis
	class MayPointToAnalysis: public DataFlowAnalysis<MayPointToInfo, true>
	{
		public : 
			// Constructor function for the class
			MayPointToAnalysis(MayPointToInfo &bottom, MayPointToInfo &initialState):DataFlowAnalysis<MayPointToInfo, true>::DataFlowAnalysis(bottom, initialState){} 

			virtual void flowfunction(Instruction *I, std::vector<unsigned> &IncomingEdges, std::vector<unsigned> &OutgoingEdges, std::vector<MayPointToInfo *> &Infos)
			{
				if(I == nullptr)
					return;
				std::string string_name = I->getOpcodeName();
				unsigned instr_index = this->InstrToIndex[I];		// Getting the index of the present instruction
				MayPointToInfo * indices_to_add = new MayPointToInfo(); 

				// Joining the information for all the incoming edges;
				for(std::vector<unsigned>::iterator i = IncomingEdges.begin(); i != IncomingEdges.end(); i++)
				{
					Edge edge = std::make_pair(*i, instr_index);
					indices_to_add = MayPointToInfo::join(EdgeToInfo[edge], indices_to_add, indices_to_add);
				}


				// Checking for all the alloca operations
				if(string_name == "alloca")
				{
					// Converting Index to string and creating IR pointers and MEM Objects
					std::string index_string = std::to_string(instr_index);
					std::string IR_string = "R" + index_string;
					std::string MEM_string = "M" + index_string;

					indices_to_add->indices[IR_string].insert(MEM_string);
					
				}

				// Checking for all the instructions opcodes which are of the first category
				else if (string_name == "bitcast" || string_name == "getelementptr")
				{
					// Converting the index into a string
					std::string index_string = std::to_string(instr_index);
					std::string IR_string = "R" + index_string;
					//where Rv is the DFA identifier of <value> so need to get Rv using getoperand
					
					Instruction * value_operand = (Instruction *)I->getOperand(0);	

					if(this->InstrToIndex.find(value_operand) != this->InstrToIndex.end())
					{
						unsigned value_index = this->InstrToIndex[value_operand];
						std::string value_string = std::to_string(value_index);
						std::string value_IR_index = "R" + value_string;

						indices_to_add->indices[index_string].insert(indices_to_add->indices[value_IR_index].begin(), indices_to_add->indices[value_IR_index].end());

					}

				}

				else if(string_name == "load")
				{
					std::string index_string = std::to_string(instr_index);
					std::string IR_string = "R" + index_string;

					if(isa<PointerType>(I->getType()))
					{
						// Using getPointerOperand of LoadInstr to find the pointer type instruction
						Instruction * load_operand = (Instruction *)((LoadInst *)I)->getPointerOperand();

						if(this->InstrToIndex.find(load_operand) != this->InstrToIndex.end())
						{
							std::string load_index_string = std::to_string(this->InstrToIndex[load_operand]);
							std::string load_IR = "R" + load_index_string;
							std::set<std::string> temp;

							for(std::set<std::string>::iterator i = indices_to_add->indices[load_IR].begin(); i != indices_to_add->indices[load_IR].end(); i++)
							{
								temp.insert(indices_to_add->indices[*i].begin(), indices_to_add->indices[*i].end());
							}
							indices_to_add->indices[IR_string].insert(temp.begin(), temp.end());
						}

					}

				}

				else if(string_name == "store")
				{
					Instruction *pointer_instr = (Instruction*)(((StoreInst*)I)->getPointerOperand());
					Instruction * value_instr = (Instruction*)(((StoreInst*)I)->getValueOperand());

					if(this->InstrToIndex.find(pointer_instr) != this->InstrToIndex.end() && this->InstrToIndex.find(value_instr) != this->InstrToIndex.end())
					{
						std::string value_string = "R" + std::to_string(this->InstrToIndex[value_instr]);
						std::string point_string = "R" + std::to_string(this->InstrToIndex[pointer_instr]);

						std::set<std::string> temp_set = indices_to_add->indices[value_string];

						for(std::set<std::string>::iterator i = indices_to_add->indices[point_string].begin(); i != indices_to_add->indices[point_string].end(); i++)
						{
							indices_to_add->indices[*i].insert(temp_set.begin(), temp_set.end());

						}
					}

				}

				else if(string_name == "select")
				{
					std::string index_string = std::to_string(instr_index);
					std::string IR_string = "R" + index_string;

					Instruction * val1 = (Instruction *) I->getOperand(1);

					if(this->InstrToIndex.find(val1) != this->InstrToIndex.end())
					{
						unsigned val1_num = this->InstrToIndex[val1];
						std::string val1_index_string = std::to_string(val1_num);
						std::string val1_IR = "R" + val1_index_string;

						indices_to_add->indices[IR_string].insert(indices_to_add->indices[val1_IR].begin(), indices_to_add->indices[val1_IR].end());

					}

					Instruction * val2 = (Instruction *) I->getOperand(2);

					if(this->InstrToIndex.find(val2) != this->InstrToIndex.end())
					{
						unsigned val2_num = this->InstrToIndex[val2];
						std::string val2_index_string = std::to_string(val2_num);
						std::string val2_IR = "R" + val2_index_string;

						indices_to_add->indices[IR_string].insert(indices_to_add->indices[val2_IR].begin(), indices_to_add->indices[val2_IR].end());

					}
				}

				else if(string_name == "phi")
				{
					unsigned temp = instr_index;

					std::string phi_index = std::to_string(instr_index);
					std::string Phi_IR = "R" + phi_index;
					while(1)
					{
						if(std::string(this->IndexToInstr[temp]->getOpcodeName()) != "phi")
							break;
						std::set<std::string> temp_set;
						Instruction * instr = this->IndexToInstr[temp];

						unsigned num_operand = instr->getNumOperands();
						for(unsigned i = 0; i < num_operand; i++)
						{
							Instruction * operand_instr = (Instruction *)instr->getOperand(i);

							if(this->InstrToIndex.find(operand_instr) != this->InstrToIndex.end())
							{
								unsigned phi_operand_index = this->InstrToIndex[operand_instr];
								std::string value_index = std::to_string(phi_operand_index);
								std::string value_IR = "R" + value_index;
								temp_set.insert(indices_to_add->indices[value_IR].begin(), indices_to_add->indices[value_IR].end());


							}
							
						}
						indices_to_add->indices[Phi_IR].insert(temp_set.begin(), temp_set.end());
						temp++;

					}

				}
				for(std::vector<MayPointToInfo *>::iterator i = Infos.begin(); i != Infos.end(); i++)
				{
					(*i)->indices = indices_to_add->indices;
				}

				delete indices_to_add;
			}



	};


	// Constructing the Pass needed for the liveness analysis
	struct MayPointToAnalysisPass:public FunctionPass{
		static char ID;
		MayPointToAnalysisPass():FunctionPass(ID){}

		// Running the function so that we call the flow fucntion
		bool runOnFunction(Function &F) override{
			MayPointToInfo bottom;		// Bottom would be an empty mapping of the pointers to objects
			MayPointToAnalysis maypointto_analysis(bottom, bottom); 

			maypointto_analysis.runWorklistAlgorithm(&F);
			maypointto_analysis.print();

			return false;


		}
	};
}

char MayPointToAnalysisPass::ID = 0;
static RegisterPass<MayPointToAnalysisPass> X("cse231-maypointto", "MayPointToAnalysisPass", false, false);





