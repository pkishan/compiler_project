/*
 * =====================================================================================
 *
 *       Filename:  opcodeCounter.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  01/21/2018 01:41:59
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Sai Kishan Pampana (Pampana), saikishanpampana@gmail.com
 *   Organization:  University of California San Diego
 *
 * =====================================================================================
 */
#include "llvm/IR/Function.h"
#include "llvm/IR/Module.h"
#include "llvm/Pass.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/IR/IRBuilder.h"
using namespace llvm;

namespace
{
	struct dinscounter:public FunctionPass
	{
		static char ID;
		dinscounter():FunctionPass(ID){}

		bool runOnFunction(Function &F) override{
			// Find out what the module and the context is to declare and insert functions.
			Module *M = F.getParent();
			LLVMContext& context = M->getContext();

			//Defining the function that we will call in the instrumented code
			Function * updateInfo = cast<Function>(M->getOrInsertFunction("updateInstrInfo", Type::getVoidTy(context), Type::getInt32Ty(context), Type::getInt32PtrTy(context), Type::getInt32PtrTy(context)));
			Function * printInfo = cast<Function>(M->getOrInsertFunction("printOutInstrInfo", Type::getVoidTy(context))); 


			for(Function::iterator basic_block = F.begin(); basic_block != F.end(); basic_block++)
			{
				std::map<unsigned int, unsigned int> counter_map;		// Op number is unsigned int 
				std::vector<unsigned> keys;					// Arrays used to send data to the updateInstrInfo function
				std::vector<unsigned> values;
				std::vector<Value*> args;					// Using array of value* to pass arguments in the CreateCall Funtion


				for(BasicBlock::iterator ins = basic_block->begin(); ins != basic_block->end(); ins++)
				{
					if(counter_map.find(ins->getOpcode()) == counter_map.end())
						counter_map[ins->getOpcode()] = 1;
					else
						counter_map[ins->getOpcode()] += 1;
					if((std::string)(ins->getOpcodeName()) == "ret")			// Printing information at the end of the function
					{
						IRBuilder<>builder(&*ins);
						builder.CreateCall(printInfo);
					}
				}

				Constant * size = ConstantInt::get(IntegerType::get(F.getContext(),32), counter_map.size());

				// Storing values in the vectors created to send to the function for updating information
				for(std::map<unsigned int, unsigned int>::iterator i = counter_map.begin(); i!= counter_map.end(); i++)
				{
					keys.push_back(i->first);
					values.push_back(i->second);
				}

				ArrayType* arrayTy = ArrayType::get(IntegerType::get(F.getContext(), 32), counter_map.size());	// Defining array type with a given size 
				GlobalVariable* args_keys = new GlobalVariable(*M, arrayTy, true, GlobalValue::InternalLinkage, ConstantDataArray::get(context, *(new ArrayRef<uint32_t>(keys))), "args_keys");
				GlobalVariable* args_values = new GlobalVariable(*M, arrayTy, true, GlobalValue::InternalLinkage, ConstantDataArray::get(context, *(new ArrayRef<uint32_t>(values))), "args_values");

				//Making a builder and casting variables to call in the function
				IRBuilder<>Builder(&*basic_block->getFirstInsertionPt());		// Inserting at the first point of the basic block
				Value* cast_args_keys = Builder.CreatePointerCast(args_keys, Type::getInt32PtrTy(context));
				Value* cast_args_values = Builder.CreatePointerCast(args_values, Type::getInt32PtrTy(context));

				//Pushing all the required arguments in the vector to pass into the CreateCall function
				args.push_back(size);
				args.push_back(cast_args_keys);
				args.push_back(cast_args_values);

				Builder.CreateCall(updateInfo, args);
			}

			
			return true;	

		}

	};
}

char dinscounter::ID = 0;
static RegisterPass<dinscounter> X("dinscounter", "Counter pass program");

