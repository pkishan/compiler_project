/*
 * =====================================================================================
 *
 *       Filename:  opcodeCounter.cpp
 *
 *    Description:  Pass to count the number of dynamic instructions that are executed  
 *
 *        Version:  1.0
 *        Created:  01/21/2018 01:41:59
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Sai Kishan Pampana (Pampana), saikishanpampana@gmail.com
 *   Organization:  University of California San Diego
 *
 * =====================================================================================
 */
#include "llvm/IR/Function.h"
#include "llvm/Pass.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Attributes.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/IR/InstrTypes.h"

using namespace llvm;

namespace
{
	struct dinscounter:public FunctionPass
	{
		static char ID;
		dinscounter():FunctionPass(ID){}

		// Function to dynamically count instructions at the run time
		bool runOnFunction(Function &F) override
		{
			std::map<std::string, int> counter_map;
			LLVMContext& Ctx = F.getContext();
			Constant* updateInstrInfo = F.getParent()->getOrInsertFunction("updateInstrInfo", Type::getVoidTy(Ctx),  NULL);		

			//std::map<std::string, int> opcode_map;
			//Iterating over the basic blocks in the function
			for(Function::iterator basic_block = F.begin(); basic_block != F.end(); basic_block++)
			{
				// Iterating over the instructions of each block to get the opcode name and adding to a map
				for(BasicBlock::iterator ins = basic_block->begin(); ins != basic_block->end(); ins++)
				{
					if(counter_map.find(ins->getOpcodeName()) == counter_map.end())
						counter_map[ins->getOpcodeName()] = 1;
					else
						counter_map[ins->getOpcodeName()] += 1;
				}
				IRBuilder<> builder(&*basic_block);
				builder.SetInsertPoint(&*basic_block);
				Value* args[] = {dyn_cast<BinaryOperator>(&*basic_block->begin())};
				builder.CreateCall(updateInstrInfo, args);

			}

			// Printing the elemnts of the map at the end of each function
			for(std::map<std::string, int>::iterator i = counter_map.begin(); i != counter_map.end(); i++)
			{
				errs() << i->first << " = " << i->second << "\n";
			}
			return true;	

		}

	};
}

char dinscounter::ID = 0;
static RegisterPass<dinscounter> X("dinscounter", "Counter pass program");

