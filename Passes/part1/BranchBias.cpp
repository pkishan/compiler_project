/*
 * =====================================================================================
 *
 *       Filename:  opcodeCounter.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  01/21/2018 01:41:59
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Sai Kishan Pampana (Pampana), saikishanpampana@gmail.com
 *   Organization:  University of California San Diego
 *
 * =====================================================================================
 */
#include "llvm/IR/Function.h"
#include "llvm/IR/Module.h"
#include "llvm/Pass.h"
#include "llvm/Support/raw_ostream.h" 
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/Instructions.h"
using namespace llvm;

namespace
{
	struct dinscounter:public FunctionPass
	{
		static char ID;
		dinscounter():FunctionPass(ID){}

		bool runOnFunction(Function &F) override{
			// Find out what the module and the context is to declare and insert functions.
			Module *M = F.getParent();
			LLVMContext& context = M->getContext();

			//Defining the function that we will call in the instrumented code
			Function * updateInfo = cast<Function>(M->getOrInsertFunction("updateBranchInfo", Type::getVoidTy(context), Type::getInt1Ty(context)));
			Function * printInfo = cast<Function>(M->getOrInsertFunction("printOutBranchInfo", Type::getVoidTy(context))); 


			for(Function::iterator basic_block = F.begin(); basic_block != F.end(); basic_block++)
			{
				std::vector<Value*> args;					// Using array of value* to pass arguments in the CreateCall Funtion


				for(BasicBlock::iterator ins = basic_block->begin(); ins != basic_block->end(); ins++)
				{
					if((std::string)(ins->getOpcodeName()) == "ret")			// Printing information at the end of the function
					{
						IRBuilder<>builder(&*ins);
						builder.CreateCall(printInfo);
					}

					if(BranchInst * branch = dyn_cast<BranchInst>(&*ins))
					{
						if(branch->isConditional()) 
						{
							Value * taken = branch->getCondition();
							args.push_back(taken);

							IRBuilder<>Builder(&*ins);
							Builder.CreateCall(updateInfo, args);

						}
					}
				}

			}


			return true;	

		}

	};
}

char dinscounter::ID = 0;
static RegisterPass<dinscounter> X("cse231-bb", "Counter pass program");

