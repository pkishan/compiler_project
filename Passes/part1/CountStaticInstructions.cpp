/*
 * =====================================================================================
 *
 *       Filename:  opcodeCounter.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  01/21/2018 01:41:59
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Sai Kishan Pampana (Pampana), saikishanpampana@gmail.com
 *   Organization:  University of California San Diego
 *
 * =====================================================================================
 */
#include "llvm/IR/Function.h"
#include "llvm/Pass.h"
#include "llvm/Support/raw_ostream.h"

using namespace llvm;

namespace
{
	struct inscounter:public FunctionPass
	{
		static char ID;
		inscounter():FunctionPass(ID){}

		bool runOnFunction(Function &F) override{
			std::map<std::string, int> counter_map;

			//std::map<std::string, int> opcode_map;
			for(Function::iterator basic_block = F.begin(); basic_block != F.end(); basic_block++)
			{
				for(BasicBlock::iterator ins = basic_block->begin(); ins != basic_block->end(); ins++)
				{
					if(counter_map.find(ins->getOpcodeName()) == counter_map.end())
						counter_map[ins->getOpcodeName()] = 1;
					else
						counter_map[ins->getOpcodeName()] += 1;
				}
			}

			for(std::map<std::string, int>::iterator i = counter_map.begin(); i != counter_map.end(); i++)
			{
				errs() << i->first << " = " << i->second << "\n";
			}
			return false;	

		}

	};
}

char inscounter::ID = 0;
static RegisterPass<inscounter> X("cse231-csi", "Counter pass program");

